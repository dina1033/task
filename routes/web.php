<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
// Route::resource('question','QuestionController');
// Route::get('/quiz/{id}/questions','QuizControlle

// Route::get('/question/create', [QuestionController::class, 'create']);

Route::resource('question', QuestionController::class);

Route::get('dynamic', function () {
    return view('task.dynamic');
})->name('dynamic');