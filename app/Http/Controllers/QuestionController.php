<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        return view('task.question.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.question.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $question = new Question;
        $question->question =$request->question;
        $question->save();

        foreach($request->options as $key=>$option){
            $is_correct =false;
            if($key==$request->correct_answer){
                $is_correct=true;
            }
            $answer = new Answer;
            $answer->question_id = $question->id;
            $answer->answer = $option;
            $answer->is_correct = $is_correct;
            $answer->save();
        }

        // \Storage::disk('local')->put($question,$answer);
        return redirect()->route('question.create')->with('message','Question created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::with('answers')->where('id',$id)->first();
        // dd($question);
        return view('task.question.show',compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::where('id',$id)->delete();
        Answer::where('question_id',$id)->delete();
        return redirect()->route('question.index')->with('message','Question deleted successfully!');
    }

    public function validateForm($request){
        return $this->validate($request,[
            'quiz'=>'required',
            'question'=>'required',
            'options'=>'required|array|min:3',
            'options.*'=>'required|string|distinct',
            'correct_answer'=>'required'
        ]);
    }

}
