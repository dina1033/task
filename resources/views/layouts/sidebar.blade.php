<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="span3">
    <div class="sidebar">
        <ul class="widget widget-menu unstyled">

            <li><a href="{{route('question.create')}}"><i class="menu-icon icon-bullhorn"></i> Create Question </a>
            </li>
            <li><a href="{{route('question.index')}}"><i class="menu-icon icon-inbox"></i>View Question <b class="label green pull-right">
                    </b> </a></li>
            <li><a href="{{route('dynamic')}}"><i class="menu-icon icon-table"></i> Dynamic Form </a>

        </ul>

        
    </div>
    <!--/.sidebar-->
</div>
