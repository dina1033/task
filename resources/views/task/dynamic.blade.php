@extends('layouts.master')

@section('title','create quiz')

@section('content')
<!-- <link rel="stylesheet" href= 
"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity= 
"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">  -->

 
<div class="span9">
    <div class="content">
        @if(Session::has('message'))
            <div class="alert alert-success">		
                {{Session::get('message')}}
            </div>
        @endif
        <div class="module">
            <div class="module-head">
                <h3>Dynamic Form</h3>
            </div>     
                <div class="module-body"> 
                    <table class="table table-bordered"> 
                        <thead> 
                        <tr> 
                            <th class="text-center">Row Number</th> 
                            <th class="text-center">Remove Row</th> 
                        </tr> 
                        
                        </thead> 
                        <tbody id="tbody"> 
                        <tr> 
                            <td class="text-center">Row 1</td> 
                            <td class="text-center"> 
                            <button class="btn btn-danger remove"
                            type="button">Remove</button> 
                            </td>
                        </tr>
                        <tr> 
                            <td class="text-center">Row 2</td> 
                            <td class="text-center"> 
                            <button class="btn btn-danger remove"
                            type="button">Remove</button> 
                            </td>
                        </tr>
                        </tbody> 
                    </table> 
                </div> 
                <button class="btn btn-md btn-primary"
                id="addBtn" type="button"> 
                    Add new Row 
                </button>  
    </div>
</div>            




<script src= 
"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"> 
</script> 
<script src= 
"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"> 
</script> 
<script src= 
"https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"> 
</script> 

<script> 
	$(document).ready(function () { 

	// Denotes total number of rows 
	var rowIdx = 2; 

	// jQuery button click event to add a row 
	$('#addBtn').on('click', function () { 

		// Adding a row inside the tbody. 
		$('#tbody').append(`<tr id="R${++rowIdx}"> 
			<td class="row-index text-center"> 
			<p>Row ${rowIdx}</p> 
			</td> 
			<td class="text-center"> 
				<button class="btn btn-danger remove"
				type="button">Remove</button> 
				</td> 
			</tr>`); 
	}); 

	$('#tbody').on('click', '.remove', function () { 

		var child = $(this).closest('tr').nextAll(); 

		child.each(function () { 
 
		var id = $(this).attr('id'); 

		var idx = $(this).children('.row-index').children('p'); 

		var dig = parseInt(id.substring(1)); 

		idx.html(`Row ${dig - 1}`); 

		$(this).attr('id', `R${dig - 1}`); 
		}); 

		$(this).closest('tr').remove(); 
		rowIdx--; 
	}); 
	}); 
</script>

@stop